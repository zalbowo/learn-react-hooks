import React from 'react'
import ReactDOM from 'react-dom/client'
import Product from './Product'
import App from './App'
import SampleApp from './useMemo/SampleApp'
import SampleCallback from './useCallback/sampleCallback'
import SampleReducer from './useReducer/SampleReducer'

ReactDOM.createRoot(document.getElementById('root')).render(
  // <React.StrictMode>
  //   {/* <Product/> */}
    
  // </React.StrictMode>,
  // <App/>
  // <SampleApp/>
  // <SampleCallback/>
  <SampleReducer/>
)
