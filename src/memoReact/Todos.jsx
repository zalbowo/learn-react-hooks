import React, {memo} from "react";

function Todos({ todos }) {

  console.warn("Render Todo")

  return (
    <>
      <h2>My Todos</h2>
      {todos.map((todo, i) => <p key={i}>{todo}</p>)}
    </>
  );
}


/// coba gunakan salah satu dari export di bawah ini, liat perbendingannya di console ////
export default memo(Todos)
// export default Todos;
