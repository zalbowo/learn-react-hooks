import React, {useReducer, memo, useRef} from 'react'

function SampleReducer() {

  const [state, dispatch] = useReducer(reducerCount, 0);

  function reducerCount(state, action) {
    // state = inital state saat ini
    // action = data yg dikirim dari dispatch

    console.log("state ", state)
    console.log("action, ", action)
    return state + action;

  }


  console.log("render")

  return (
    <div>

      <p>{state}</p>

      <button type='button' onClick={() => dispatch(1)}>Pluss +1</button>

    </div>
  )
}

export default memo(SampleReducer)