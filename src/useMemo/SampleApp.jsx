import React, { useState, useMemo } from 'react'

function SampleApp() {
  const [count, setCount] = useState(0);
  const [todos, setTodos] = useState(["todo1", "todo2"]);
  const calculation = useMemo(() => heavyCalculation(count), [count]);

  function heavyCalculation(num) {
    console.log("Calculating...");
    for (let i = 0; i < 1000000000; i++) {
      num += 1;
    }
    return num;
  }

  function addTodo() {
    setTodos([...todos, "new todo"])
  }

  function increment() {
    setCount(count + 2);
  }


  return (
    <>
      <div>
        <h2>My Todos</h2>
        {todos.map((todo, index) => {
          return <p key={index}>{todo}</p>;
        })}
        <button onClick={addTodo}>Add Todo</button>
      </div>
      <hr />

      <div>
        Count: {count}
        <button onClick={increment}>+</button>
        <h2>Expensive Calculation</h2>
        {calculation}
      </div>
    </>
  );
}

export default SampleApp