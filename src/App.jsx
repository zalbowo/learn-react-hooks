import React, { useState, memo } from 'react'
import Todos from './memoReact/Todos'

export default function App() {
  const [count, setCount] = useState(1);
  const [todos, setTodos] = useState(["todo 1", "todo 2"]);

  function handleUpdate() {
    setTodos([...todos, "New Todo"])
  }

  console.log("render App")
  return (
    <div>
      <Todos todos={todos}/>
      <hr />
      <div>
        Count: {count}
        <br />
        <button onClick={() => setCount(count + 1)}>plus 1</button>
        <button onClick={handleUpdate}>Add Todo</button>
      </div>
    </div>
  )
}
 