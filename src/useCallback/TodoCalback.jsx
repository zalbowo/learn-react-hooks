import React, {memo} from "react";

function TodoCalback({ todos, handleAddTodo }) {
  //   useEffect(() => {
  //     const greeting = greet();
  //     console.log(greeting);
  //   }, [handleAddTodo]);

  //   const greet = useCallback(() => {
  //     return "Halo ini fungsi greet";
  //   }, [handleAddTodo]);

  //   function greet() {
  //     return "Halo ini fungsi greet";

  //   }

  console.warn("Render Todo");

  return (
    <>
      <h2>My Todos</h2>
      {todos.map((todo, i) => (
        <p key={i}>{todo}</p>
      ))}

      <div>
        <button onClick={handleAddTodo}>Add Todo</button>
      </div>
    </>
  );
}

export default memo(TodoCalback);
