import React, { useState, useCallback } from "react";
import TodoCalback from "./TodoCalback";

function SampleCallback() {
  const [count, setCount] = useState(1);
  const [todos, setTodos] = useState(["todo 1", "todo 2"]);


  // bandingankan kedua function tsb dan cek console //

//   function handleUpdate() {
//     setTodos([...todos, "New Todo"]);
//   }

  const handleUpdate = useCallback(() => {
    setTodos([...todos, "New Todo"]);
  }, [todos])

  console.log("render App");
  return (
    <div>
      <TodoCalback todos={todos} handleAddTodo={handleUpdate} />
      <hr />
      <div>
        Count: {count}
        <br />
        <button onClick={() => setCount(count + 1)}>plus 1</button>
      </div>
    </div>
  );
}

export default SampleCallback;
